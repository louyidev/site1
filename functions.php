<?php


/**
 * @return string The current URL
 */
function getCurrentURL() {

    // $_SERVER est une variable superglobale, "remplie" par Apache, au moment ou il a démarré PHP
    // Le "problème" c'est que $_SERVER['REQUEST_URI'] contient le dossier dans lequel on a rangé le site sur WAMP / MAMP.
    // Ce qui n'est pas très utile pour nous ici, donc on l'enlève avec la fonction str_replace

    $currentUrl = str_replace(BASE_FOLDER, "", $_SERVER['REQUEST_URI']);
    $currentUrl = str_replace("/public", "", $currentUrl);

    return $currentUrl;
}