<div class = "service-container">

    <div class = "service-menu-bloc">
        <div class = "service-title-menu">SERVICES
            <div class ="service-title-ser"></div>
        </div>

        <div class = "service-menu">
            <ul>
                <a class= "interior-button" href="">
                    <li>
                        <div class="service-item-logo"><img src="img/interior.svg" width="30" height="30" alt=""> </div>
                        <div class="service-item-text">&ensp; INTERIOR</div>
                    </li>
                </a>
                <a class= "concepts-button" href="">
                    <li>
                        <div class="service-item-logo"><img src="img/ideas.svg" width="30" height="30" alt=""> </div>
                        <div class="service-item-text">&ensp; CONCEPTS </div>
                    </li>
                </a>
                <a class= "residential-button" href="">
                    <li>
                        <div class="service-item-logo"><img src="img/modern-house.svg" width="30" height="30" alt=""> </div>
                        <div class="service-item-text">&ensp; RESIDENTIAL </div>
                    </li>
                </a>
                <a class= "hospitality-button"href="">
                    <li>
                        <div class="service-item-logo"><img src="img/skyline.svg" width="30" height="30" alt=""> </div>
                        <div class="service-item-text">&ensp; HOSPITALITY </div>
                    </li>
                </a>

            </ul>
        </div>
    </div>

    <div class = "service-description-bloc1">

        <div class = "service-description-box"></div>

        <div class = "service-description-logo">
            <img src="img/interior.svg" width="100" alt="">
        </div>
        <span class="interior-indicator1"></span>

        <div class = "service-description-title">Interior Design</div>

        <div class = "service-description-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis temporibus optio doloremque minima at veniam officiis dolor praesentium iure reprehenderit?</div>

        <button class = "service-description-button">LEARN MORE</button>

    </div>


    <div class = "service-description-bloc2">

        <div class = "service-description-box"></div>

        <div class = "service-description-logo">
            <img src="img/ideas.svg" width="100" alt="">
        </div>
        <span class="interior-indicator2"></span>

        <div class = "service-description-title">Concept Design</div>

        <div class = "service-description-text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In quia alias obcaecati ab deserunt enim corporis eligendi, corrupti temporibus tempora!</div>

        <button class = "service-description-button">LEARN MORE</button>

    </div>


    <div class = "service-description-bloc3">

        <div class = "service-description-box"></div>

        <div class = "service-description-logo">
            <img src="img/modern-house.svg" width="100" alt="">
        </div>
        <span class="interior-indicator3"></span>

        <div class = "service-description-title">Residential</div>

        <div class = "service-description-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Error dolorum consequuntur aperiam totam molestias similique dignissimos praesentium id blanditiis quis!</div>

        <button class = "service-description-button">LEARN MORE</button>

    </div>


    <div class = "service-description-bloc4">

        <div class = "service-description-box"></div>

        <div class = "service-description-logo">
            <img src="img/skyline.svg" width="100" alt="">
        </div>
        <span class="interior-indicator4"></span>

        <div class = "service-description-title">Hospitality</div>

        <div class = "service-description-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam aliquid illum sint explicabo quaerat modi iure, voluptas aperiam culpa similique!</div>

        <button class = "service-description-button">LEARN MORE</button>

    </div>

</div>
