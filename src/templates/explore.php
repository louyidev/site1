<div class = "explore-container">

    <div class = "explore-text-container">

        <div class = "explore-text-container-bloc" >
            <div class = "text-bloc">
                <div class = "number-explore">800</div>
                <div class = "text-explore">FINISHED PROJECTS </div>
            </div>
            <div class = "text-bloc">
                <div class = "number-explore">795</div>
                <div class = "text-explore">HAPPY CUSTOMERS </div>
            </div></div>

        <div  class = "explore-text-container-bloc">
            <div class = "text-bloc">
                <div class = "number-explore">1200</div>
                <div class = "text-explore">WORKING HOURS </div>
            </div>
            <div class = "text-bloc">
                <div class = "number-explore">850</div>
                <div class = "text-explore">CUPS OF COFFEE</div>
            </div></div>


    </div>

    <div class = "explore-image-container">
        <img src="img/image_1.jpg" min-height="400" alt="building">
        <button class ="button-explor">Explor Further </button>
    </div>

</div>