
<div class = "carroussel-container">

    <a class ="menu-button" href="">
        <nav>
            <span class ="span1"></span>
            <span class ="span2"></span>
            <span class ="span3"></span>
        </nav>
    </a>

    <?php

    $currentUrl = getCurrentURL();

    ?>


    <div class = "menu-bloc">

        <ul>
            <?php if ($currentUrl === '/'): ?>
                <li><a href="" class="active">Home</a></li>
            <?php else:?>
                <li><a href="" class="">Home</a><li>
            <?php endif ?>

            <li><a href="services" class = "<?= $currentUrl === '/services' ? 'active' : ""?>" >Services</a></li>
            <li><a href="about" class = "<?= $currentUrl === '/about' ? 'active' : ""?>" >About</a></li>
            <li><a href="portfolio" class = "<?= $currentUrl === '/portfolio' ? 'active' : ""?>" >Portfolio</a></li>
            <li><a href="projects" class = "<?= $currentUrl === '/projects' ? 'active' : ""?>" >Projects</a></li>
            <li><a href="contact" class = "<?= $currentUrl === '/contact' ? 'active' : ""?>" >Contact</a></li>

        </ul>

    </div>

    <img class="img-carrousel_1" src="img/pexels-photo-1106476.jpeg"  height="700" width="633" alt="building1">
    <img class="img-carrousel_2"  src="img/pexels-photo-1534411.jpeg"  height="700" width="633" alt="building2">
    <img class="img-carrousel_3" src="img/pexels-photo-1650904.jpeg"  height="700" width="633" alt="building3">

    <div class="title-header">[TOUGH]</div>
    <div class="title-header1">#01 AMERICAN BUILDING</div>
    <div class="title-header2">#02 LOUIE'S RESIDENCES</div>
    <div class="title-header3">#03 BUSINESS BUILDING</div>
</div>