<?php
include "../config.php"
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">

    <title>Services</title>
</head>
<body>

<?php
include "../src/templates/header.php"
?>

<?php
include "../src/templates/services.php"
?>

<?php
include "../src/templates/footer.php"
?>

<script src="js/index.js"></script>

</body>
</html>