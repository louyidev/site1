



<?php

// Remplir la table feedback d'une centaine témoignages les plus aléatoires possibles :
// (une dizaine d'auteurs différents, qui ont chacun une dizaine de témoignages)
// cinq ou six jobs différentss
// si possible, faire varier les dates de creation
//

// -------
// EXEMPLE D'ALGO:

// definir un tableau de quelques noms
// definir un tableau de quelques métiers
// definir un tableau de mots aleatoires, de messages aleatoires
// Créer un dizaine de vraies personnes aux noms uniques : [nom, job]
// rajouter une personne ayant le même prénom mais un boulot différent
// Pour chacune de ces personnes, créer une dizaine de témoignages aleatoires

// -------


$names = ['riri', 'fifi', 'loulou', 'donald', 'picous', "D'artagnan"];
$jobs = ['architect', 'worker', 'boss'];
$contents = [
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque fugit minus, molestias quas quasi veniam?',
    'Debitis eum odit optio placeat repellendus tempora.',
    'Explicabo necessitatibus provident quas suscipit ullam, velit.',
    'Cumque dolore eaque earum est iure recusandae?',
    'Dignissimos dolorem eius pariatur quia voluptates, voluptatum?',
    'A enim fugit necessitatibus neque officia ratione?',
    'Fuga inventore nostrum voluptas. Autem, eos, voluptas!',

];


// utilisation de array_rand
$randomIndex = array_rand($names);
$randomName = $names[$randomIndex];

// Ou alors avec une fonction "custom"
function getRandom($array) {

    $index = rand(0, count($array) - 1);

    $elt = $array[$index];

    return $elt;
}


function createFeedback() {

    global $names, $jobs, $contents; // utilisation d'une variable globale à l'intérieur d'une fonction

    return [
        'author' => getRandom($names),
        'job' => getRandom($jobs),
        'content' => getRandom($contents),
    ];
}


require_once  "../config.php";
require_once  "./../src/database/sql.php";

// l'inconvénient d'une boucle de 100, c'est que chaque ligne est indépendante des autres
// et donc "trop" aleatoire.
for ($i = 0; $i < 100; $i++) {

    // créer un feedback
    $feedback = createFeedback();

    // -----------------------------------
    // l' enregistrer
    $name = $feedback['author'];
    $job = $feedback['job'];
    $content = $feedback['content'];

    $sql = "INSERT INTO feedbacks (author, job, content) VALUES ('$name', '$job', '$content')";

    $result = $database->exec($sql);
    // -----------------------------------
}
