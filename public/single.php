<?php
include "../config.php";


$projectId = $_GET['work'];





?>

    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">

        <title>Single</title>
    </head>
    <body>

    <?php
    include "../src/templates/header.php"
    ?>

    <div class="container">
        <h2>Work #<?= $projectId ?></h2>
        <img src="img/image_<?php echo $projectId ?>.jpg" alt="">

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi cupiditate esse laboriosam molestiae nesciunt porro quam, quod repudiandae. A animi cupiditate, debitis delectus dicta ea eum iste laborum libero modi mollitia odio, odit pariatur qui rem unde veritatis. Consequuntur delectus doloremque dolorum est, et exercitationem expedita explicabo laboriosam laudantium minus nesciunt non odio porro quae quaerat quia quibusdam quis recusandae reiciendis rem, repellendus sequi sit sunt suscipit unde veniam voluptate! A aliquam at, culpa enim error ex inventore ipsam ipsum maiores modi numquam, officiis praesentium quasi qui quis quisquam ratione repellat saepe sapiente tempora tempore, ut vel vitae voluptates voluptatibus?</p>
    </div>



    <?php
    include "../src/templates/footer.php"
    ?>
    <script src="js/index.js"></script>

    </body>
    </html>

