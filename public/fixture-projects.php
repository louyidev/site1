<?php

$titles = ['Le couteau des abeilles ', 'Lopacité du colonel ', 'La venue des hirondelles ', 'La torture des tubes ', 'Le cercueil des faibles ', "Les cauchemars des bouchers "];
$summarys = [
    'Cum dolorem enim eum ratione saepe tempore.',
    'A expedita fugit obcaecati provident, sunt veniam.',
    'Commodi deleniti hic, id magnam non perspiciatis!',
    'Debitis dolore ipsa ipsum, magni totam unde?',
];

$descriptions = [
    'Frappez à cette porte que passent les mois sans leur en ajouter un qui lui parle par la fenêtre de toute la splendeur de léclair, oscilla, puis tout seffaçait, se noyait, confuse. Frileusement emmitouflée dans sa grande main, et les sceptiques lemportaient, cétait ridicule, vraiment...  ',
    'Frileusement emmitouflée dans sa grande main, et les sceptiques lemportaient, cétait ridicule, vraiment... Contemplez mon oeuvre, cest tuer son essence, laquelle néanmoins vous deviez principalement prouver, et nous te rejoindrons bientôt.',
    'Contemplez mon oeuvre, cest tuer son essence, laquelle néanmoins vous deviez principalement prouver, et nous te rejoindrons bientôt. Hachée et soulevée par le vent ou les arbres superflus ou de ne pas revenir en même temps rire et être élevé ? ',
    'Aujourdhui nous écrivons les lettres, et quand mon oncle me réveilla, non par des choses plus importantes à faire. Contemplez mon oeuvre, cest tuer son essence, laquelle néanmoins vous deviez principalement prouver, et nous te rejoindrons bientôt.',
    'Mas-tu pardonné un crime apparent dont le destin immanent était connu de tout le jour à la chasse, des coutumes, les préférences sont frappantes, directes. Sentez comme mon coeur bat ; puis, à la ville éternelle, où il laissa à sa fille pour la dernière alternative était la plus insultante enquête !',
    'Sentez comme mon coeur bat ; puis, à la ville éternelle, où il laissa à sa fille pour la dernière alternative était la plus insultante enquête ! Contemplez mon oeuvre, cest tuer son essence, laquelle néanmoins vous deviez principalement prouver, et nous te rejoindrons bientôt.',
    'Contemplez mon oeuvre, cest tuer son essence, laquelle néanmoins vous deviez principalement prouver, et nous te rejoindrons bientôt.Aujourdhui nous écrivons les lettres, et quand mon oncle me réveilla, non par des choses plus importantes à faire.  ',

];

$image = [

]

// utilisation de array_rand
$randomIndex = array_rand($titles);
$randomTitle = $titles[$randomIndex];

// Ou alors avec une fonction "custom"
function getRandom($array) {

    $index = rand(0, count($array) - 1);

    $elt = $array[$index];

    return $elt;
}

function createProject() {

    global $titles, $summarys, $descriptions; // utilisation d'une variable globale à l'intérieur d'une fonction

    return [
        'title' => getRandom($titles),
        'summary' => getRandom($summarys),
        'description' => getRandom($descriptions),
    ];
}


require_once  "../config.php";
require_once  "./../src/database/sql.php";

// l'inconvénient d'une boucle de 10, c'est que chaque ligne est indépendante des autres
// et donc "trop" aleatoire.
for ($i = 0; $i < 10; $i++) {

    // créer un project
    $project = createProject();

    // -----------------------------------
    // l' enregistrer
    $title = $project['title'];
    $summary = $project['summary'];
    $description = $project['description'];

    $sql = "INSERT INTO projects (title, summary, description, image) VALUES ('$title', '$summary', '$description', '$image')";
    echo ($sql);


    $result = $database->exec($sql);
    // -----------------------------------
}