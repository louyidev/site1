<?php
require_once __DIR__ . "/../src/database/sql.php";
$results = $database->query("SELECT * FROM feedbacks ORDER BY rand() LIMIT 2");
$feedbacks = $results->fetchAll(PDO::FETCH_ASSOC);

?>

<div class = "client-service-container">

    <div class = "client-say-box">
        <div class="client-say-title">CLIENT SAYS</div>
        <div class="client-say-text">Far far away, behind the word montains, far from the countries vokalia
        </div>
    </div>

    <div class = "testimony">

        <div class = "testimony-bloc">
            <div class="avatar"><img src="img/person_2.jpg" height="100" width="100"alt="avatar-men"></div><br>
            <div class="testimony-text"><?php echo $feedbacks[0]['content'];?> </div>
            <div class="testimony-name"><?php echo $feedbacks[0]['author'];?> </div>
            <div class="testimony-work"><?php echo $feedbacks[0]['job'];?> </div>
        </div>

        <div class = "testimony-bloc">
            <div class="avatar"><img src="img/person_3.jpg" height="100" width="100"alt="avatar-men"></div><br>
            <div class="testimony-text"><?php echo $feedbacks[1]['content'] ?></div>
            <div class="testimony-name"><?php echo $feedbacks[1]['author'];?></div>
            <div class="testimony-work"><?php echo $feedbacks[1]['job'];?></div>
        </div>

    </div>

</div>
