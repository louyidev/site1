<?php

include "../config.php";


// Page 1 => 1 - 10
// Page 2 => 11 - 20
// Page 3 => 21 - 30
// ...
// Page 10 => 91 - 100


define('MAX_PER_PAGE', 100);

$nbProjects = 124; // nombre totalement arbitraire ^^

if (isset($_GET['perPage']) && is_numeric($_GET['perPage'])) {
    $perPage = intval($_GET['perPage']); // force la page a être un nombre entier

    if ($perPage < 10) {
        $perPage = 10;
    } else if ($perPage > MAX_PER_PAGE) {
        $perPage = MAX_PER_PAGE;
    }
} else {
    $perPage = 10;
}

$nbPages = ceil($nbProjects / $perPage);


if (isset($_GET['p']) && is_numeric($_GET['p'])) {
    $page = intval($_GET['p']); // force la page a être un nombre entier

    if ($page < 1) {
        $page = 1;
    }

    if ($page > $nbPages) {
        $page = $nbPages;
    }

} else {
    $page = 1;
}

// Définition des "bornes" de la boucle. Cet algo fonctionne mais n'est pas très élégant !
$end = $page * $perPage;
$start = $end - ($perPage - 1);
if ($end > $nbProjects) {
    $start = ($nbPages - 1) * $perPage;
    $end = $start + $nbProjects % $perPage;
}

$pageTitle = "Portfolio (page $page)";
$pageDescription= "Check our awesome projects and realizations...";
$pageClassName = "page-portfolio";

?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/portfolio-grid.css">

    <title>About</title>

</head>
<body>

<?php
include "../src/templates/header.php"
?>




<h2>PORTFOLIO</h2>

<?php include "../src/templates/portfolio-grid.php"  ?>



<div class="pagination">

    <?php

    // config de la pagination
    $nextPage = $page + 1;
    $nextPage = "$currentUrl?p=$nextPage&perPage=$perPage";
    $prevPage = $page - 1;
    $prevPage = "$currentUrl?p=$prevPage&perPage=$perPage";

    require_once "./../src/templates/pagination/prev-next.php"
    ?>

    <?php require_once "./../src/templates/pagination/pagination.php" ?>

</div>

<?php require_once "./../src/templates/pagination/results-per-page.php" ?>


<?php
include "../src/templates/footer.php"
?>

<script src="js/index.js"></script>

</body>
</html>
