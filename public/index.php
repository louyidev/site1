<?php
include "../config.php"
?>

<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />



    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">


    <title>EPCF 1 - Front end</title>
</head>

<body>


<div class = "master-container">


        <!--BLOC HEADER -->

     <?php
     include "../src/templates/header.php"
     ?>

        <!--EXPLORE BLOC  -->

    <?php
    include "../src/templates/explore.php"
    ?>

        <!--SERVICE BLOC  -->

    <?php
    include "../src/templates/services.php"
    ?>

        <!--PORTFOLIO BLOC  -->

     <?php
     include "../src/templates/portfolio.php"
     ?>

        <!--REVIEWS BLOC  -->

    <?php
    include "reviews.php"
    ?>

        <!--BUSINESS SELL BLOC-->

    <?php
    include "../src/templates/business.php"
    ?>

        <!--FOOTER -->

    <?php
    include "../src/templates/footer.php"
    ?>

</div>

<script src="js/index.js"></script>

</body>
</html>
