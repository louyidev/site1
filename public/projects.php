<?php
include "../config.php";

require_once __DIR__ . "/../src/database/sql.php";
$results = $database->query("SELECT * FROM projects ORDER BY rand() LIMIT 2");
$projects = $results->fetchAll(PDO::FETCH_ASSOC);

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">

    <title>Projects</title>
</head>
<body>

<?php
include "../src/templates/header.php"
?>

<div class = "project-bloc">
    <h2><div class="project-title">PROJET : <?php echo $projects[0]['title'];?> </div></h2>
    <h3><div class="project-summary"><?php echo $projects[0]['summary'];?> </div></h3>
    <div class="project-description"><?php echo $projects[0]['description'];?> </div>
    <div class="project-image"><?php echo $projects[0]['image'];?> </div>

</div>


<?php
include "../src/templates/footer.php"
?>

<script src="js/index.js"></script>

</body>
</html>

