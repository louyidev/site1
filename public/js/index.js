document.addEventListener("DOMContentLoaded", function () {

    /* BURGER MENU */
    var $menuBloc = document.querySelector(".menu-bloc")
    var $menuButton = document.querySelector(".menu-button")
    
    $menuButton.addEventListener("click", function(){
        event.preventDefault();
        if ($menuBloc.style.display === "block"){
            $menuBloc.style.display = "none";
        }  else $menuBloc.style.display = "block";    
    });

    /* SELECTION ABOUT SERVICE MENU */
    var $bloc1 = document.querySelector(".service-description-bloc1")
    var $bloc2 = document.querySelector(".service-description-bloc2")
    var $bloc3 = document.querySelector(".service-description-bloc3")
    var $bloc4 = document.querySelector(".service-description-bloc4")
    
    var $interiorButton = document.querySelector(".interior-button")
    var $conceptsButton = document.querySelector(".concepts-button")
    var $residentialButton = document.querySelector(".residential-button")
    var $hospitalityButton = document.querySelector(".hospitality-button")
    
    /* DISPLAY BLOC AND ARROW WITH MENU*/
    $interiorButton.addEventListener("click", function () {
        event.preventDefault();
        $bloc1.style.display = "block";
        $bloc2.style.display = "none";
        $bloc3.style.display = "none";
        $bloc4.style.display = "none";
    });
   
    $conceptsButton.addEventListener("click", function () {
        event.preventDefault();
        $bloc2.style.display = "block";
        $bloc1.style.display = "none";
        $bloc3.style.display = "none";
        $bloc4.style.display = "none";
    });

    $residentialButton.addEventListener("click", function () {
        event.preventDefault();
        $bloc3.style.display = "block";
        $bloc1.style.display = "none";
        $bloc2.style.display = "none";
        $bloc4.style.display = "none";
    });

    $hospitalityButton.addEventListener("click", function () {
        event.preventDefault();
        $bloc4.style.display = "block";
        $bloc1.style.display = "none";
        $bloc2.style.display = "none";
        $bloc3.style.display = "none";
    });

});