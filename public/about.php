<?php
include "../config.php"
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">

    <title>About</title>
</head>
<body>
<?php
include "../src/templates/header.php"
?>
<h2>ABOUT US</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis dolore dolores doloribus eaque expedita, facilis fuga fugit, minus modi quae quaerat quasi quos repudiandae tempora veritatis. Amet inventore, minima odio quisquam ratione sapiente temporibus voluptas! Aperiam autem corporis, deleniti eos fugiat fugit nobis quam sit totam voluptas? Consequuntur ducimus impedit magnam, odit pariatur quaerat quis tenetur ut velit. Accusantium, aliquid aperiam blanditiis debitis dicta, ea eaque eligendi expedita fugit harum illum incidunt inventore itaque, laboriosam nisi quisquam recusandae repellat reprehenderit similique unde. Ab illum maxime optio suscipit unde veritatis! Dicta doloribus earum fugiat odio, quisquam recusandae sapiente voluptate voluptatem voluptatum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias harum illum in magni quis quos velit. Asperiores debitis deleniti dignissimos est eum harum, id incidunt nihil numquam obcaecati omnis porro quae, quis rerum saepe sapiente similique? Dolore eaque eius est eum eveniet explicabo incidunt iusto magni modi molestias quae repudiandae soluta, sunt suscipit tempora temporibus vero? A architecto blanditiis commodi consequatur corporis cum debitis, deleniti distinctio earum eligendi enim eos exercitationem itaque, iusto laudantium libero magni maxime nemo nisi optio porro praesentium quibusdam quidem sint veniam, voluptate voluptatibus voluptatum? Consectetur consequatur dolores perspiciatis praesentium quaerat quibusdam veniam! Animi consequuntur debitis delectus eaque fuga, incidunt neque non nulla perspiciatis porro recusandae, sed totam veritatis vero vitae. Ex, harum, impedit. At corporis culpa distinctio, hic id illum laborum laudantium officiis possimus quis quo ratione recusandae repellat sunt tenetur voluptatem voluptatum? Atque consectetur cupiditate, id illo mollitia necessitatibus officiis quae repellendus tempora veniam.</p>

<?php
include "../src/templates/footer.php"
?>

<script src="js/index.js"></script>





</body>
</html>